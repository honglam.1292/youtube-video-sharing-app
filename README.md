<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://youtube-video-sharing-app-seven.vercel.app/">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Youtube Video Sharing App</h3>

  <p align="center">
    An awesome app to share your youtube video!
    <br />
    </a>
    <br />
    <br />
    <a href="https://youtube-video-sharing-app-seven.vercel.app/">View Demo</a>
  </p>
</div>


<!-- ABOUT THE PROJECT -->
## About The Project

<img src="https://img.upanh.tv/2023/07/19/Screenshot-2023-07-19-at-00.49.31.png" alt="Logo" width="200" height="100">
<img src="https://img.upanh.tv/2023/07/19/Screenshot-2023-07-19-at-00.49.11.png
" alt="Logo" width="200" height="100">
<img src="https://img.upanh.tv/2023/07/19/Screenshot-2023-07-19-at-00.49.01.png
" alt="Logo" width="200" height="100">
<img src="https://img.upanh.tv/2023/07/19/Screenshot-2023-07-19-at-00.48.34.png" alt="Logo" width="200" height="100">



Feature:
* User registration and login
* Sharing YouTube videos
* iewing a list of shared videos (no need to display up/down votes)


### Built With

This project is created by CRA, using antd as a UI lib

## Getting Started

This is an example of how you setting up your project locally.

### Installation

_Below is an example of how you can instruct your audience on installing and setting up your app. This template doesn't rely on any external dependencies or services._

1. Clone the repo
   ```sh
   git clone https://gitlab.com/honglam.1292/youtube-video-sharing-app.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
4. Start the project
    ```sh
   npm start
   ```

