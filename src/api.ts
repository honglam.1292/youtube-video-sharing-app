//todo move to env file
const BASE_URL = 'https://64b2af9238e74e386d556f7e.mockapi.io'

const API = {
	get: async (url: string) => {
		const rs = await fetch(`${BASE_URL}/${url}`, {
			method: 'GET',
			headers: { 'content-type': 'application/json' },
		})
		return rs.json()
	},
	post: async (url: string, body: { [key: string]: any }) => {
		const rs = await fetch(`${BASE_URL}/${url}`, {
			method: 'POST',
			headers: { 'content-type': 'application/json' },
			// Send your data in the request body as JSON
			body: JSON.stringify(body)
		})
		return rs.json();
	},
	put: async (url: string, body: { [key: string]: any }) => {
		const rs = await fetch(`${BASE_URL}/${url}`, {
			method: 'PUT',
			headers: { 'content-type': 'application/json' },
			// Send your data in the request body as JSON
			body: JSON.stringify(body)
		})
		return rs.json();
	}
}
export default API;