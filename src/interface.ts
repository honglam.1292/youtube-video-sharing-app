export interface User {
  username?: string;
  password?: string;
}
export interface IUserContext {
  user: string;
  setUser: any;
}
export interface IVideo {
  id: number;
  url: string;
  username: string;
  like: string[];
  unlike: string[];
}
export interface IMovie {
  userName: string;
  video: IVideo;
}
