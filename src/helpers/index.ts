import { User } from "../interface";

const CURRENT_USER = 'current_user';
const SHARED_VIDEOS = 'shared_videos'


export const getUser = (): string => {
  if (localStorage.getItem(CURRENT_USER)) {
    return JSON.parse(localStorage.getItem(CURRENT_USER) || '');
  }
  return '';
}

export const logout = () => {
  localStorage.removeItem(CURRENT_USER);
}
