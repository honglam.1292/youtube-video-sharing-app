import React, { useState } from "react";
import { HomeOutlined } from "@ant-design/icons";
import { Input, Button, Form, Space, Modal, message } from "antd";
import { User } from "../../interface";
import { logout } from "../../helpers";
import { useUserContext } from "../../App";
import { useNavigate } from "react-router-dom";
import { HeaderText, HeaderContainer, RightContent } from "./styles";
import { createUser, isValidUser } from "../../services/user";

interface HeaderProps {}

const Header = ({}: HeaderProps) => {
  const { user, setUser } = useUserContext();
  const [isRegisterModalOpen, setIsRegisterModalOpen] = useState(false);
  const [registerForm] = Form.useForm();
  const [messageApi, contextHolder] = message.useMessage();
  const toggleModal = () => {
    setIsRegisterModalOpen(!isRegisterModalOpen);
  };
  const navigate = useNavigate();

  const onFinish = async (values: User) => {
    const isValid = await isValidUser(values);
    if (isValid) {
      setUser(values.username);
    } else {
      messageApi.open({
        type: "error",
        content: "Invalid user name or password",
      });
    }
  };
  const onRegister = async (values: User) => {
    const newUser: User = await createUser(values);
    if (newUser.username)
      messageApi.open({
        type: "success",
        content: `User ${newUser.username} has registered successfully`,
      });
    toggleModal();
  };
  const handleLogout = () => {
    logout();
    setUser(null);
  };
  const navigateSharePage = () => {
    navigate("share");
  };
  const navigateHome = () => {
    navigate("details");
  };
  return (
    <HeaderContainer>
      {contextHolder}
      <Modal
        title="Register"
        onOk={registerForm.submit}
        open={isRegisterModalOpen}
        onCancel={() => setIsRegisterModalOpen(false)}
      >
        <Form form={registerForm} onFinish={onRegister} autoComplete="off">
          <Form.Item name="username">
            <Input placeholder="User Name" />
          </Form.Item>
          <Form.Item name="password">
            <Input.Password placeholder="Password" />
          </Form.Item>
        </Form>
      </Modal>
      {!user ? (
        <>
          <div>
            <HomeOutlined style={{ fontSize: "48px" }} />{" "}
            <HeaderText onClick={navigateHome}>Funny Movie</HeaderText>
          </div>
          <RightContent>
            <Form
              name="login"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
              initialValues={{ remember: true }}
              onFinish={onFinish}
              autoComplete="off"
            >
              <Form.Item name="username" className="inline-block">
                <Input placeholder="User Name" />
              </Form.Item>
              <Form.Item name="password" className="inline-block">
                <Input.Password placeholder="Password" />
              </Form.Item>
              <Form.Item className="inline-block">
                <Button type="primary" htmlType="submit">
                  Login
                </Button>
              </Form.Item>
              <Button type="primary" className="ml-1" onClick={toggleModal}>
                Register
              </Button>
            </Form>
          </RightContent>
        </>
      ) : (
        <>
          <div>
            <HomeOutlined style={{ fontSize: "48px" }} />{" "}
            <HeaderText onClick={navigateHome}>Funny Movie</HeaderText>
          </div>
          <RightContent>
            <Space>
              <div>Welcome {user}</div>
              <Button type="primary" onClick={navigateSharePage}>
                Share a movie
              </Button>
              <Button type="primary" onClick={handleLogout}>
                Logout
              </Button>
            </Space>
          </RightContent>
        </>
      )}
    </HeaderContainer>
  );
};

export default Header;
