import styled from "styled-components";

export const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  border-bottom: 1px solid #d0d0d0d9;
  .inline-block {
    display: inline-block;
  }
  .ml-1 {
    margin-left: 1rem;
  }
`;

export const HeaderText = styled.span`
  font-size: 32px;
`;

export const RightContent = styled.div`
  padding-top: 8px;
  flex-basis: 600px;
`;
