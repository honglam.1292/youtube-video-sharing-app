import React from "react";
import Body from "../Body";
import Header from "../Header";
import { AppLayoutContainer } from "./styles";

const AppLayout = () => (
  <AppLayoutContainer>
    <Header></Header>
    <Body></Body>
  </AppLayoutContainer>
);

export default AppLayout;
