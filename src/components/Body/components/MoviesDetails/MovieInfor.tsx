import React, { useEffect, useState } from "react";
import { Space } from "antd";
import {
  LikeOutlined,
  DislikeOutlined,
  LikeFilled,
  DislikeFilled,
} from "@ant-design/icons";
import { updateLike } from "../../../../services/video";
import { useUserContext } from "../../../../App";
import { Title } from "../styles";
import { IMovie } from "../../../../interface";

enum Status {
  LIKE = "LIKE",
  UNLIKE = "UNLIKE",
  DEFAULT = "",
}

const MovieInfor = ({ userName, video }: IMovie) => {
  const [status, setStatus] = useState("");
  const { user } = useUserContext();

  const handleLike = () => {
    setStatus(Status.LIKE);
    const likeArr = video.like;
    likeArr.push(user);
    updateLike(video.id, {
      like: likeArr,
      unlike: video.unlike,
    });
  };

  const handleDisLike = () => {
    setStatus(Status.UNLIKE);
    const unLikeArr = video.unlike;
    unLikeArr.push(user);
    updateLike(video.id, {
      like: video.like,
      unlike: unLikeArr,
    });
  };
  
  useEffect(() => {
    if (video.like.includes(user)) {
      setStatus(Status.LIKE);
    }
    if (video.unlike.includes(user)) {
      setStatus(Status.UNLIKE);
    }
  }, [user]);
  return (
    <>
      <Title>Movie Title</Title>
      <div>
        <Space direction="horizontal" size="small">
          <>Share by: {userName}</>
          {user ? (
            <>
              {status === Status.DEFAULT && (
                <>
                  <LikeOutlined
                    style={{ fontSize: "32px" }}
                    onClick={handleLike}
                  />
                  <DislikeOutlined
                    style={{ fontSize: "32px" }}
                    onClick={handleDisLike}
                  />
                  (un-voted)
                </>
              )}
              {status === Status.LIKE && (
                <>
                  <LikeFilled style={{ fontSize: "32px" }} /> (Voted up)
                </>
              )}
              {status === Status.UNLIKE && (
                <>
                  <DislikeFilled style={{ fontSize: "32px" }} /> (Voted down)
                </>
              )}
            </>
          ) : null}
        </Space>
      </div>
      <Space direction="horizontal" size="small">
        <>
          12
          <LikeOutlined style={{ fontSize: "16px" }} />{" "}
        </>
        <>
          {" "}
          34
          <DislikeOutlined style={{ fontSize: "16px" }} />
        </>
      </Space>
      <div>Description:</div>
      <br />
      <div>
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry's standard dummy text ever
        since the 1500s, when an unknown printer took a galley of type and
        scrambled it to make a type specimen book.
      </div>
    </>
  );
};

export default MovieInfor;
