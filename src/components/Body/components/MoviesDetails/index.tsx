import React, { useEffect, useState } from "react";
import { Col, Row } from "antd";
import { getVideo } from "../../../../services/video";
import MovieInfor from "./MovieInfor";
import { IVideo } from "../../../../interface";

const Body = () => {
  const [videoList, setVideoList] = useState<IVideo[]>([]);
  const getVideoList = async () => {
    const list = await getVideo();
    setVideoList(list);
  };
  useEffect(() => {
    getVideoList();
  }, []);
  return (
    <>
      {videoList.map((video, i) => (
        <Row key={`${video.url}${i}`}>
          <Col span={12} className="right">
            <iframe
              width="320"
              height="180"
              src={video.url}
              title="YouTube video player"
            ></iframe>
          </Col>
          <Col span={12}>
            <MovieInfor userName={video.username} video={video} />
          </Col>
        </Row>
      ))}
    </>
  );
};

export default Body;
