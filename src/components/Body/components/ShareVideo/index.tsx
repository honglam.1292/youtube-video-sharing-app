import React from "react";
import { Button, Form, Input } from "antd";
import { useNavigate } from "react-router-dom";
import { addVideo } from "../../../../services/video";
import { ShareContainer } from "./styles";
import { useUserContext } from "../../../../App";

const ShareVideo = () => {
  const navigate = useNavigate();
  const { user } = useUserContext();

  const onFinish = (values: { link: string }) => {
    if (user) {
      const videoID = values.link.split("?v=")[1];
      const url = `https://www.youtube.com/embed/${videoID}`;
      addVideo(url, user);
      navigate("/details");
    }
  };

  return (
    <ShareContainer>
      <Form name="share" onFinish={onFinish} autoComplete="off">
        <Form.Item
          label="Youtube url"
          name="link"
          rules={[{ required: true, message: "Please input your link!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Share
          </Button>
        </Form.Item>
      </Form>
    </ShareContainer>
  );
};

export default ShareVideo;
