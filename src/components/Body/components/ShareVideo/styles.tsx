import styled from "styled-components";

export const ShareContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 5rem;
  form {
    width: 500px;
  }
`;
