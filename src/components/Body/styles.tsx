import styled from "styled-components";

export const BodyContainer = styled.div`
  .ant-col {
    padding: 16px;
  }
  .right {
    text-align: right;
  }
`;
