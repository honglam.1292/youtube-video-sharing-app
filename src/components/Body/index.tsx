import React from "react";
import { Col, Row } from "antd";
import { BodyContainer } from "./styles";
import { Routes, Route } from "react-router-dom";
import MovieDetails from "./components/MoviesDetails";
import ShareVideo from "./components/ShareVideo";

const Body = () => {
  return (
    <BodyContainer>
      <Routes>
        <Route path="/" element={<MovieDetails />} />
        <Route path="details" element={<MovieDetails />} />
        <Route path="share" element={<ShareVideo />} />
      </Routes>
    </BodyContainer>
  );
};

export default Body;
