import React, { useContext, useEffect, useState } from "react";
import "./App.css";
import Layout from "./components/Layout";
import { createContext } from "react";
import { getUser } from "./helpers";
import { User, IUserContext } from "./interface";
import { BrowserRouter } from "react-router-dom";

const UserContext = createContext<IUserContext>({
  user: "",
  setUser: () => {},
});
export const useUserContext = () => useContext<IUserContext>(UserContext);

function App() {
  const [user, setUser] = useState<string>(getUser());
  return (
    <UserContext.Provider value={{ user, setUser }}>
      <BrowserRouter>
        <Layout />
      </BrowserRouter>
    </UserContext.Provider>
  );
}

export default App;
