import API from "../api";
import { User } from "../interface";

export const createUser = async (user: User) => {
	return API.post('user', user);
}
export const isValidUser = async (user: User) => {
	const listUser: User[] = await API.get('user');
	const existingUser = listUser.find(u => u.username === user.username && u.password === user.password)
	return !!existingUser;
}