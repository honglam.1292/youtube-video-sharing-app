import API from "../api";

export const addVideo = async (url: string, username: string) => {
	return API.post('video', { url, username, like: [], unlike: [] });
}
export const getVideo = async () => {
	const listVideo = await API.get('video');
	return listVideo;
}
export const updateLike = async (id: number, body: { like: string[], unlike: string[] }) => {
	const updatedVideo = await API.put(`video/${id}`, body);
}